const csvToStudents = (csvData) => {
    if(!csvData) {
        return [];
    }
    
    const lines = csvData.split("\n");
    const students = [];

    for (const line of lines) {
        const [lastname, firstname, email, ..._] = line.split("\t");
        if (!lastname || !firstname || !email) {
            continue;
        }

        students.push({
            name: lastname,
            firstname,
            email,
        });
    }

    return students;
}

export {
    csvToStudents
}