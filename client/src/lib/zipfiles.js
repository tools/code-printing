const zipFileNamesFromDataTransfer = (dataTransfer) => {
    const names = [];

    if (!dataTransfer) {
        return names;
    }

    if (dataTransfer.items) {
        // Use DataTransferItemList interface to access the file(s)
        for (var i = 0; i < dataTransfer.items.length; i++) {
            // If dropped items aren't files, reject them
            if (dataTransfer.items[i].kind === 'file') {
                var file = dataTransfer.items[i].getAsFile();
                names.push(file.name)
                // console.log('... file[' + i + '].name = ' + file.name);
            }
        }
    } else {
        // Use DataTransfer interface to access the file(s)
        for (var i = 0; i < dataTransfer.files.length; i++) {
            names.push(dataTransfer.files[i].name)
        }
    }

    return names;
}

const fileListToFiles = (fileList) => {
    const zipFiles = [];


    if (fileList) {
        // Use DataTransferItemList interface to access the file(s)
        for (var i = 0; i < fileList.length; i++) {
            // If dropped items aren't files, reject them
            if (!fileList[i].kind || fileList[i].kind === 'file') {
                var file = fileList.item(i);
                zipFiles.push(file)
                // console.log('... file[' + i + '].name = ' + file.name);
            }
        }
    } else {
        // Use DataTransfer interface to access the file(s)
        for (var i = 0; i < dataTransfer.files.length; i++) {
            zipFiles.push(dataTransfer.files[i])
        }
    }



    return zipFiles;
}

const zipFilesFromDataTransfer = (dataTransfer) => {
    const zipFiles = [];

    if (!dataTransfer) {
        return zipFiles;
    }

    zipFiles.push(...fileListToFiles(dataTransfer.files));
    return zipFiles
}

export {
    zipFilesFromDataTransfer,
    fileListToFiles
}