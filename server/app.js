const express = require('express')
const fs = require('fs');
const path = require('path');
const cors = require('cors')
const util = require('util');
const puppeteer = require('puppeteer');
const { execSync } = require("child_process");


var JSZip = require("jszip");
const app = express()
const port = 3002



app.use(cors());
app.use(express.json({limit: '500mb'}));

app.use('/api/code', express.static(path.join(__dirname, 'out')))


const print = async (archives) => {
  for (archive of archives) {
    const user = archive.email;

      const browser = await puppeteer.launch({
        ignoreHTTPSErrors: true,
        // headless: false,
      });

    for (file of archive.files) {
      url = `http://localhost:3000/?code=${user}/${file.name}`
      console.log("file", file)
      console.log("url", url)

      filePath = `./out/${user}/${file.name}.pdf`

      console.log("processing", filePath);




      const page = await browser.newPage();

      
      await page.setViewport({
        width: 3440,
        height: 1440,
        deviceScaleFactor: 1,
      });

      await page.goto(url, { waitUntil: 'networkidle0' });
      // await page.waitForTimeout(50000);

      /*
      await page.addStyleTag(
          {
              content: "@page { size: 300mm 1200mm }; .container-limited { max-width: 5000px !important; }"
          }
      )
      */

      /*
      await page.addStyleTag(
        {
          path: "style.css"
        }
      )
      */


      const userNoEmail = user.replace(/@.*$/, '');
      await page.pdf({ path: filePath, width: '400mm', height: '5000mm' });
      execSync(`pdf-crop-margins -v -p 0 -a -16 ${filePath} -o ./cropped/${userNoEmail}_${file.name}.pdf`)
    }
      await browser.close();
  }

}

app.post('/api/upload', async (req, res) => {
  const { data, archives } = req.body;

  var zip = await JSZip.loadAsync(data, { base64: true });
  console.log("archives", archives);

  await new Promise((resolve) => {
    zip
      .generateNodeStream({ type: 'nodebuffer', streamFiles: true })
      .pipe(fs.createWriteStream('out.zip'))
      .on('finish', function () {
        console.log("out.zip written.");
        resolve();
      });
  });


  await execSync("./extract.sh");

  await print(archives);

  return res.send("ok");
})

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})
