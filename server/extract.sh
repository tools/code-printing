#!/bin/bash

cd `dirname $0`
rm -rf out
rm -rf cropped
mkdir out
mkdir cropped
cd out
unzip ../out.zip